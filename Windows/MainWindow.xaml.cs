﻿using System.Windows;
using System.Windows.Input;
using WPFWeather.ViewModel;

namespace WPFWeather.Windows
{
    public partial class MainWindow : Window
    {
        private WeatherViewModel viewModel;
        private string[] Cities = { "Brno", "London", "Praha"};
        private int index = 0;
        public MainWindow()
        {
            InitializeComponent();
            viewModel = new WeatherViewModel();
            viewModel.DownloadWeather(Cities[index]);
            DataContext = viewModel;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            index++;
            index %= 3;
            viewModel.DownloadWeather(Cities[index]);
        }
    }
}
